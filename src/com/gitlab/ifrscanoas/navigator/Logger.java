/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.ifrscanoas.navigator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

/**
 *
 * @author marcio
 */
public class Logger {

    private File f;
    private FileWriter fw;

    public Logger() {
        f = new File("log.txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(String str) {
        try {
             fw.append("\n { data : '" + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date())+ "', registro: '"+ str +"'},");
             fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        super.finalize(); 
    }
}
